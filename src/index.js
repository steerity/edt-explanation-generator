const _ = require('lodash')

const linkingExpressions = {
    lessThanPrevious: ['Par contre', 'Néanmoins'],
    equalThanPrevious: ['De même', 'Egalement'],
    betterThanPrevious: ['En revanche', 'Cependant', 'Au contraire'],
}

function computeSuccessLevel(score) {
    return Math.floor(score / 20)
}

function getExplanationsForCategory(category, explanations) {
    const explanation = explanations.find(exp => exp.category === category)

    if (!explanation) {
        return ''
    }

    return explanation.sentences
}

function computeExplanationText(results, explanations) {
    return _.map(results, ({ category, score }, index) => {
        const successLevel = computeSuccessLevel(score)

        const texts = getExplanationsForCategory(category, explanations)
        const text = texts[_.clamp(successLevel, 0, texts.length - 1)]

        if (index !== 0) {
            const previousSuccessLevel = computeSuccessLevel(
                results[index - 1].score
            )
            const prefixCase = _.clamp(
                successLevel - previousSuccessLevel,
                -1,
                1
            )

            let prefix = ''
            switch (prefixCase) {
                case -1:
                    prefix = _.shuffle(linkingExpressions.lessThanPrevious)[0]
                    break
                case 1:
                    computeExplanationText
                    prefix = _.shuffle(linkingExpressions.betterThanPrevious)[0]
                    break
                default:
                    prefix = _.shuffle(linkingExpressions.equalThanPrevious)[0]
                    break
            }

            return prefix + ', ' + _.lowerFirst(text).trim()
        }

        return text
    }).join(' ')
}

module.exports = {
    computeExplanationText,
}
